# Docker container build

This is more or less the build script I used last time

```
apt-get install -y git build-essential devscripts libkrb5-dev openssl g++ libssl-dev libcap-dev libtool m4 automake libcppunit-dev libcppunit-doc pkg-config python-polib python3-polib default-jre libpam-dev npm python-lxml python3-lxml translate-toolkit docker.io locales-all libpng16-16 fontconfig adduser cpio findutils nano libpoco-dev libcap2-bin openssl inotify-tools procps
apt-get build-dep -y libreoffice
npm i npm@latest -g

getent passwd lool || useradd -s /bin/bash -m lool
adduser lool docker

grep lool /etc/sudoers || echo "lool	ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

su lool <<EOF
cd /opt
git clone http://gitlab.fuss.bz.it/fuss-team/fuss-online.git
cd /opt/fuss-online/docker
CORE_BRANCH="libreoffice-6-4-1" ONLINE_BRANCH="fuss-online-6-4" FUSS_STANDARD_ARGS="yes" NO_DOCKER_PUSH="yes" ./l10n-docker-nightly.sh 
EOF
```
